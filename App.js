import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Login from './src/pages/Login';
import LoginStyled from './src/pages/LoginStyled';
import Register from './src/pages/Register';
import RegisterStyled from './src/pages/RegisterStyled';
import SplashScreen from './src/pages/SplashScreen';

import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer} from '@react-navigation/native'


export default function App() {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown:false}}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="LoginStyled" component={LoginStyled} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="RegisterStyled" component={RegisterStyled} />  

      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
