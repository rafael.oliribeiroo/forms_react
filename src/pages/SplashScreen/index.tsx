import React from 'react';
import { Text, ImageBackground, View, TouchableOpacity, TextInput } from 'react-native';
import { LoginButton, LoginButtonText, BackGroundApp, LogoApp} from './styles';
import Icon from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';

export default function SplashScreen() {

    const Navigation = useNavigation();

    return (
        <BackGroundApp>            

        
            <View>
            <LogoApp source={require('../../../assets/logoApp.png')}></LogoApp>
            </View>                 
            
            {/*Botão para começar*/} 
            <LoginButton onPress={() => Navigation.navigate('Login')}>
                <LoginButtonText>Começar</LoginButtonText>
            </LoginButton>

        </BackGroundApp>
    )
};