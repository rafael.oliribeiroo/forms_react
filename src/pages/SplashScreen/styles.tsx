import styled from 'styled-components/native';
import {widthPercentageToDP as vw, heightPercentageToDP as vh} from 'react-native-responsive-screen';

export const BackGroundApp = styled.View `
    resizeMode: contain; 
    backgroundColor: #3e484f;

    height: 100%;
    width: 100% 
`;

export const LoginButton = styled.TouchableOpacity `
    width: 90%;
    padding: 4%;
   
    background-color: #3bf6de;
    opacity: 0.7;

    
    box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.25);
    
    margin: 12% 6%;  
`;

export const LoginButtonText = styled.Text `

 
    font-weight: bold;
    color: #3e484f;
    font-size: 17px;
    font-family: Roboto;

    text-align:Center;  

`;

export const LogoApp = styled.ImageBackground `

    margin: auto;
    margin-top: 45%;

    resizeMode: contain;

    height: ${vh('22%')};
    width: ${vw('60%')};  
`;


