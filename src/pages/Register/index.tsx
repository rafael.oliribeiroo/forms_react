import React from 'react';
import { ButtonPage } from '../LoginStyled/styles';
import { Text, View, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { useForm, Controller } from 'react-hook-form';
import { RadioButton } from 'react-native-paper';
import { TextInputMask } from 'react-native-masked-text';
import { useNavigation } from '@react-navigation/native';

export default function Register() {

    const { control, getValues, handleSubmit, formState: { errors }} = useForm({ mode: 'onTouched' });
    const onError = (errors: Object) => { console.log(errors) };

    const Navigation = useNavigation();

    interface FormData {
        name: string,
        email: string,
        birthdate: string,
        password: string,
        passwordConfirmation: string,
        gender: string,
    }

    const onSubmit = (data: FormData) => { 
        console.log(data);      
    }  

    return (
        <View>
          
            
        </View>                    
    );

}
