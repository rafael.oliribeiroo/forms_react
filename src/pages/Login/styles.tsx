import styled from 'styled-components/native';

export const ButtonPage = styled.View `
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    margin: 5% 0;
`;