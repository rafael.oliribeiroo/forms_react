// Importações não necessárias para a aula
import { useNavigation } from '@react-navigation/native';
import { ButtonPage } from '../LoginStyled/styles';
import Icon from 'react-native-vector-icons/Feather';


// Importações utilizadas e necessárias para a aula
import React from 'react';
import { Text, View, TouchableOpacity, TextInput, Alert} from 'react-native';
import { useForm, Controller } from 'react-hook-form';



export default function Register() {

    const { control, handleSubmit, formState: { errors }} = useForm({ mode: 'onTouched' });

    const onError = (errors: Object) => { console.log(errors) };

    const Navigation = useNavigation();

    interface FormData {
        email: string;
        password: string;
    }   

    const onSubmit = (data: FormData) => { 
        console.log(data);      
    }  

    return (
        <View>
            {/*botão* mudança de página*/}
            <ButtonPage>
                {/*Seta para voltar*/} 
                <TouchableOpacity onPress={() => Navigation.navigate('SplashScreen')}>
                    <Icon name="arrow-left-circle" size={35} color="#3e484f"/>
                </TouchableOpacity>  
                {/*Seta para avançar */}
                <TouchableOpacity onPress={() => Navigation.navigate('LoginStyled')}>
                    <Icon name="arrow-right-circle" size={35} color="#3e484f"/>
                </TouchableOpacity>
            </ButtonPage>           
            
            {/*Texto principal do Formulário */} 
            <View>
                <Text>Login</Text>
            </View>            

            {/*Input Email */} 
            <View>
                <Text>E-mail</Text>
                <Controller
                    control={control}
                    render={({field:{onBlur, onChange, value}}) => (
                        <TextInput
                            placeholder='E-mail'
                            onBlur= {onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                        />
                    )}
                    rules={{
                        required: 'O e-mail é obrigatório.',
                        pattern: {
                            value: /^\S+@\S+$/i,
                            message: 'Formato de e-mail inválido.'
                        },
                    }}
                    name='email'
                    defaultValue=''
                />
                {errors.email && <Text style={{ color: 'red' }}>{errors.email.message}</Text>}
            </View>

            {/*Input Senha */}                 
            <View>
                <Text>Senha</Text>
                <Controller
                    control={control}
                    render={({field:{onBlur, onChange, value}}) => (
                        <TextInput
                            placeholder='senha'
                            secureTextEntry
                            onBlur= {onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                        />
                    )}
                    rules={{ 
                        required: 'A senha é obrigatória.',
                        minLength: {
                            value: 4,
                            message:'A senha deve conter, no mínimo, 4 dígitos',
                        }
                    }}
                    name='password'
                    defaultValue=''
                />
                {errors.password && <Text style={{ color: 'red' }}>{errors.password.message}</Text>}
            </View>

            {/*Botão para envio do Formulário*/} 
            <TouchableOpacity onPress={handleSubmit(onSubmit, onError)}>
                <Text>Entrar</Text>
            </TouchableOpacity> 

        </View>
    )
}










