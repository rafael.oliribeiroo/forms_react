import React from 'react';
import { TitleGenderRadio, BoxForm, LoginButtonText, LoginButton, Input, TitleForm, ButtonPage, BackGroundApp} from './styles';
import { Text, View, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { useForm, Controller } from 'react-hook-form';
import { RadioButton } from 'react-native-paper';
import { TextInputMask } from 'react-native-masked-text';
import { useNavigation } from '@react-navigation/native';

export default function Register() {

    const { control, getValues, handleSubmit, formState: { errors }} = useForm({ mode: 'onTouched' });
    const onError = (errors: Object) => { console.log(errors) };

    const Navigation = useNavigation();

    interface FormData {
        name: string,
        email: string,
        birthdate: string,
        password: string,
        passwordConfirmation: string,
        gender: string,
    }

    const onSubmit = (data: FormData) => { 
        console.log(data);      
    }  

    return (
        <BackGroundApp>
            <ButtonPage>
                {/*Seta para voltar*/} 
                <TouchableOpacity onPress={() => Navigation.navigate('Register')}>
                    <Icon name="arrow-left-circle" size={35} color="#3bf6de"/>
                </TouchableOpacity>  
            </ButtonPage>
            
            {/*Texto principal do Formulário */} 
            <View>
            <TitleForm>Register</TitleForm>
            </View>            

            {/*Input Email */} 
            
            <View>
                <Controller
                    control={control}
                    render={({field:{onBlur, onChange, value}}) => (
                        <Input style={{outline: "none", color:"white"}}
                            placeholder='Nome'
                            autoCorrect={false}
                            keyboardType='email-address'
                            textContentType='name'
                            onBlur= {onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                        />
                    )}
                    rules={{
                        required: 'O nome é obrigatório.',
                    }}
                    name='name'
                    defaultValue=''
                />
                {errors.name && <Text style={{ color: '#f24f4f', marginLeft:'5%' }}>{errors.name.message}</Text>}
            </View>

            {/*Input Email */} 
            <View>
                <Controller
                    control={control}
                    render={({field:{onBlur, onChange, value}}) => (
                        <Input style={{outline: "none", color:"white"}}
                            placeholder='E-mail'
                            autoCompleteType='email'
                            autoCorrect={false}
                            keyboardType='email-address'
                            textContentType='emailAddress'
                            onBlur= {onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                        />
                    )}
                    rules={{
                        required: 'O e-mail é obrigatório.',
                        pattern: {
                            value: /^\S+@\S+$/i,
                            message: 'Formato de e-mail inválido.'
                        },
                    }}
                    name='email'
                    defaultValue=''
                />
                {errors.email && <Text style={{ color: '#f24f4f', marginLeft:'5%' }}>{errors.email.message}</Text>}
            </View>

            {/*Input Data de Nascimento - utilizando a bibiolteca Masked-Text */}                
            <View>
                <Controller
                    control={control}
                    render={({field:{onChange, value}}) => (
                        <Input style={{outline: "none", color:"white"}}
                            placeholder='Aniversário: DD/MM/AAAA'
                            type={'datetime'}
                            keyboardType='numeric'                           
                            options={{
                                format: 'DD/MM/YYYY'
                            }}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                        />
                    )}
                    rules={{
                        required: 'A data de nascimento é obrigatória.',                        
                    }}
                    name='birthdate'
                    defaultValue=''
                />
                {errors.birthdate && <Text style={{ color: '#f24f4f', marginLeft:'5%' }}>{errors.birthdate.message}</Text>}
            </View>   

            {/*Input Senha */}                 
            <View>
                <Controller
                    control={control}
                    render={({field:{onChange, value}}) => (
                        <Input style={{outline: "none", color:"white"}}
                            placeholder="Senha"
                            secureTextEntry
                            autoCompleteType='password'
                            autoCorrect={false}
                            textContentType='password'                            
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                        />
                    )}
                    rules={{ 
                        required: 'A senha é obrigatória.' 
                    }}
                    name='password'
                    defaultValue=''
                />
                {errors.password && <Text style={{ color: '#f24f4f', marginLeft:'5%' }}>{errors.password.message}</Text>}
            </View>

            {/*Input Confirmar Senha */}                 
            <View>
                <Controller
                    control={control}
                    render={({field: {onBlur, onChange, value}}) => (
                        <Input style={{outline: "none", color:"white"}}
                            placeholder="Confirmar senha"
                            secureTextEntry
                            autoCompleteType='password'
                            autoCorrect={false}
                            textContentType='password'
                            onBlur= {onBlur}
                            onChangeText={(value:any) => onChange(value)}
                            value={value}
                        />
                    )}
                    rules={{ 
                        required: 'A confirmação de senha é obrigatória.',
                        validate: {
                            matchesPreviousPassword: (value) => {
                                const { password } = getValues();
                                return password === value || 'As senhas não coincidem.';
                            }
                        }

                    }}
                    name='passwordConfirmation'
                    defaultValue=''
                />
                {errors.passwordConfirmation && <Text style={{ color: '#f24f4f', marginLeft:'5%' }}>{errors.passwordConfirmation.message}</Text>}
            </View>
            

            {/*Radio Input - Gênero */}
            <View>
                <TitleGenderRadio>Gênero</TitleGenderRadio>
                <Controller
                    control={control}
                    render={({field:{onChange, value}}) => (
                        <View>
                            <RadioButton.Group onValueChange={(value) => onChange(value)} value={value}>
                                <RadioButton.Item color='#32CFE3' style={{width: '90%', margin:'auto'}} label="Feminino" labelStyle={{color:"white"}} value="feminino" />
                                <RadioButton.Item color='#32CFE3' style={{width: '90%', margin:'auto'}} label="Masculino" labelStyle={{color:"white"}} value="masculino" />
                                <RadioButton.Item color='#32CFE3' style={{width: '90%', margin:'auto'}} label="Outro" labelStyle={{color:"white"}} value="outro" />
                            </RadioButton.Group>
                        </View>
                    )}
                    rules={{ 
                        required: 'Esse campo é obrigatório.',
                    }}
                    name='gender'
                    defaultValue=''
                />
                {errors.gender && <Text style={{ color: 'red', marginLeft:'5%' }}>{errors.gender.message}</Text>}
            </View>
            
            {/*Botão para envio do Formulário*/} 
            <LoginButton onPress={handleSubmit(onSubmit, onError)}>
                <LoginButtonText>Entrar</LoginButtonText>
            </LoginButton> 
            
        </BackGroundApp>                    
    );

}
