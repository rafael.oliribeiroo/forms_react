import React from 'react';
import { Text, View, TouchableOpacity, TextInput, ImageBackground, CheckBox} from 'react-native';
import { LoginButtonText, LoginButton, Input, TitleForm, ButtonPage, BackGroundApp} from './styles';
import { useForm, Controller } from 'react-hook-form';

//importações não necessárias para a aula
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Feather';


export default function Register() {

    const { control, handleSubmit,  formState: { errors }} = useForm({ mode: 'onTouched' });
    const onError = (errors: Object) => { console.log(errors) };

    const Navigation = useNavigation();

    interface FormData {
        email: string;
        password: string;
    }

    const onSubmit = (data: FormData) => { 
        console.log(data);      
    }  

    return (
        <BackGroundApp>

            <ButtonPage>
                {/*Seta para voltar*/} 
                <TouchableOpacity onPress={() => Navigation.navigate('Login')}>
                    <Icon name="arrow-left-circle" size={35} color="#3bf6de"/>
                </TouchableOpacity>  
                {/*Seta para avançar */}
                <TouchableOpacity onPress={() => Navigation.navigate('Register')}>
                    <Icon name="arrow-right-circle" size={35} color="#3bf6de"/>
                </TouchableOpacity>
            </ButtonPage>  
            
            {/*Texto principal do Formulário */} 
            <View>
                <TitleForm>Login</TitleForm>
            </View>            

            {/*Input Email */} 
           
                <View>
                    <Controller
                        control={control}
                        render={({field:{onBlur, onChange, value}}) => (
                            <Input style={{outline: "none", color:"white"}}
                                placeholder='E-mail'
                                autoCompleteType='email'
                                autoCorrect={false}
                                keyboardType='email-address'
                                textContentType='emailAddress'
                                onBlur= {onBlur}
                                onChangeText={(value:any) => onChange(value)}
                                value={value}
                            />
                        )}
                        rules={{
                            required: 'O e-mail é obrigatório.',
                            pattern: {
                                value: /^\S+@\S+$/i,
                                message: 'Formato de e-mail inválido.'
                            },
                        }}
                        name='email'
                        defaultValue=''
                    />
                    {errors.email && <Text style={{ color: '#f24f4f', marginLeft:'5%' }}>{errors.email.message}</Text>}
                </View>

                {/*Input Senha */}                 
                <View>
                    <Controller
                        control={control}
                        render={({field:{onBlur, onChange, value}}) => (
                            <Input style={{outline: "none", color:"white"}}
                                placeholder='senha'
                                secureTextEntry
                                autoCompleteType='password'
                                autoCorrect={false}
                                textContentType='password'
                                onBlur= {onBlur}
                                onChangeText={(value:any) => onChange(value)}
                                value={value}
                            />
                        )}
                        rules={{ 
                            required: 'A senha é obrigatória.' 
                        }}
                        name='password'
                        defaultValue=''
                    />
                    {errors.password && <Text style={{ color: '#f24f4f', marginLeft:'5%'}}>{errors.password.message}</Text>}
                </View>
            

            {/*Botão para envio do Formulário*/} 
            <LoginButton onPress={handleSubmit(onSubmit, onError)}>
                <LoginButtonText>Entrar</LoginButtonText>
            </LoginButton> 

        </BackGroundApp>
    )
}