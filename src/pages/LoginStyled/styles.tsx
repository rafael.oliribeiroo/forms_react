import styled from 'styled-components/native';
import {widthPercentageToDP as vw, heightPercentageToDP as vh} from 'react-native-responsive-screen';

export const BackGroundApp = styled.View `
    resizeMode: contain; 
    backgroundColor: #3e484f;

    height: 100%;
    width: 100% 
`;

export const ButtonPage = styled.View `
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    margin: 5% 0;
`;

export const TitleForm = styled.Text `
    font-weight: bold;
    color: white;
    font-size: 25px;
    font-family: Roboto;

    text-align: center;
    margin: 7% 0;
`;

export const Input = styled.TextInput `
        
    width: 90%;
    padding: 4%;
    margin: 0.5% auto;

    background-color: black;
    opacity: 0.3;
    
    borderBottomWidth: 1;
    borderBottomColor: #ffff;
`;

export const LoginButton = styled.TouchableOpacity `
    width: 90%;
    padding: 4%;
    background-color: #3bf6de;
    opacity: 0.7;

    
    box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.25);
    
    margin: 5% 6%;  
`;

export const LoginButtonText = styled.Text `

    font-weight: bold;
    color: #3e484f;;
    font-size: 17px;
    font-family: Roboto;

    text-align:Center;  

`;